let mensajes = ["Hola", "Hola 2", "Hola 3"]
const miDependencia = () => {
    const mesaje = mensajes[Math.floor(Math.random() * mensajes.length)];
    console.log(`\x1b[34m${mesaje}\x1b[89m`);
  }
  
  module.exports = {
    miDependencia
  };
